/* global $, window */

$(function () {
    $(document).on("change", "select[name=orderBy]", function () {
        var sortValue = $(this).val();
        sortTable($('.table.table-striped:eq(0)'), sortValue);

    });

    $('#fileupload').bind('fileuploadcompleted', function (e, data) {
        sortTable($('.table.table-striped:eq(0)'), $("select[name=orderBy]").val());
    });

});

function sortTable(table, sortBy) {
    var tbody = table.find('tbody');

    tbody.find('tr').sort(function (a, b) {
        switch (sortBy) {
            case 'date-desc':
                return $('td:eq(3)', b).text().localeCompare($('td:eq(3)', a).text());
                break;
            case 'date-asc':
                return $('td:eq(3)', a).text().localeCompare($('td:eq(3)', b).text());
                break;
            case 'name-desc':
                return $('td:eq(2)', b).text().localeCompare($('td:eq(2)', a).text(), 'cs', {sensitivity: 'case', caseFirst: 'upper'});
                break;
            default:
                return $('td:eq(2)', a).text().localeCompare($('td:eq(2)', b).text(), 'cs', {sensitivity: 'case', caseFirst: 'upper'});
        }
    }).appendTo(tbody);
}
