<?php

/**
 * Extending class of Jquery-file-upload. Adds property of creation date to files list
 * @author Tomas Plachy <plachy.t@gmail.com>
 * created: 13.06.2018
 */
class ExtendedUploadHandler extends UploadHandler
{

    /**
     * overloading of original method, adds handle for download of selected files
     */
    protected function initialize()
    {
        switch ($this->get_server_var('REQUEST_METHOD')) {
            case 'OPTIONS':
            case 'HEAD':
                $this->head();
                break;
            case 'GET':
                if ($this->get_query_param('_method') === 'downloadZipFile' && $this->get_query_param('filename') != "") {
                    $zip_path = $this->options['upload_dir'] . $this->get_query_param('filename');
                    $zip_name = $this->get_query_param('filename');
                    $this->downloadZipFile($zip_path, $zip_name);
                    break;
                } elseif ($this->get_query_param('_method') === 'downloadZipFile') {
                    $this->header('HTTP/1.1 404 Not Found');
                    break;
                }
                $this->get($this->options['print_response']);
                break;
            case 'PATCH':
            case 'PUT':
            case 'POST':
                if ($this->get_query_param('_method') === 'downloadSelected') {
                    $this->downloadSelected($this->options['param_name']);
                    break;
                }
                $this->post($this->options['print_response']);
                break;
            case 'DELETE':
                $this->delete($this->options['print_response']);
                break;
            default:
                $this->header('HTTP/1.1 405 Method Not Allowed');
        }
    }

    /**
     * handle for action download all selected files
     * @param type $filesKey
     */
    protected function downloadSelected($filesKey)
    {
        $files = $this->get_post_param($filesKey);
        $zip_name = $this->generateZipFile($files);
//        echo $this->options['upload_url'] . $zip_name;
        echo $zip_name;
    }

    /**
     * generates zip from files saved in upload folder by input array list of files
     * @param array $files
     * @return string zip name
     * @throws Exception
     */
    protected function generateZipFile($files)
    {
        if (empty($files)) {
            throw new Exception("cant create zip file, because input files are empty");
        }
        $zip = new ZipArchive();
        $zip_name = uniqid('zip_archive_') . ".zip";
        $zip_path = $this->options['upload_dir'] . $zip_name;
        if (file_exists($zip_path)) {
            unlink($zip_path);
        }
        $zip->open($zip_path, ZIPARCHIVE::CREATE);

        foreach ($files as $key => $file) {
            $file_path = $this->get_upload_path($file);
            $zip->addFile($file_path, $file);
        }
        $zip->close();
        return $zip_name;
    }

    /**
     * returns zip file saved in upload folder
     * @param string $file_path
     * @param string $file_name
     * @throws Exception
     */
    protected function downloadZipFile($file_path,
        $file_name)
    {
        if (file_exists($file_path)) {

            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: application/zip");
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:" . filesize($file_path));
            header("Content-Disposition: attachment; filename=$file_name");
            readfile($file_path);
            unlink($file_path);
            die();
        } else {
            throw new Exception("Zip file not found.");
        }
    }

    /**
     * overloads original function, puts datetime to file list, used for generating list of files
     * @param string $file_name
     * @return \stdClass
     */
    protected function get_file_object($file_name)
    {
        if ($this->is_valid_file_object($file_name)) {
            $file = new stdClass();
            $file->name = $file_name;
            // get date modified of file
            $file->date = new DateTime();
            $file->date->setTimestamp(filemtime($this->get_upload_path($file_name)));
            $file->dateFormated = $file->date->format('d.m.Y H:i:s');
            $file->size = $this->get_file_size($this->get_upload_path($file_name));
            $file->url = $this->get_download_url($file->name);
            foreach ($this->options['image_versions'] as $version => $options) {
                if (!empty($version)) {
                    if (is_file($this->get_upload_path($file_name, $version))) {
                        $file->{$version . 'Url'} = $this->get_download_url(
                            $file->name, $version
                        );
                    }
                }
            }
            $this->set_additional_file_properties($file);
            return $file;
        }
        return null;
    }

    /**
     * overloads original function, puts datetime to file list, used for uploading new files
     * @param type $uploaded_file
     * @param type $name
     * @param type $size
     * @param type $type
     * @param type $error
     * @param type $index
     * @param type $content_range
     * @return \stdClass
     */
    protected function handle_file_upload($uploaded_file,
        $name,
        $size,
        $type,
        $error,
        $index = null,
        $content_range = null)
    {
        $file = new stdClass();
        $file->name = $this->get_file_name($uploaded_file, $name, $size, $type, $error, $index, $content_range);
        $file->size = $this->fix_integer_overflow((int) $size);
        $file->type = $type;
        if ($this->validate($uploaded_file, $file, $error, $index)) {
            $this->handle_form_data($file, $index);
            $upload_dir = $this->get_upload_path();
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, $this->options['mkdir_mode'], true);
            }
            $file_path = $this->get_upload_path($file->name);
            $append_file = $content_range && is_file($file_path) &&
                $file->size > $this->get_file_size($file_path);
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file_path, fopen($uploaded_file, 'r'), FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path, fopen($this->options['input_stream'], 'r'), $append_file ? FILE_APPEND : 0
                );
            }
            $file_size = $this->get_file_size($file_path, $append_file);
            // get date modified of file
            $file->date = new DateTime();
            $file->date->setTimestamp(filemtime($file_path));
            $file->dateFormated = $file->date->format('d.m.Y H:i:s');
            if ($file_size === $file->size) {
                $file->url = $this->get_download_url($file->name);
                if ($this->is_valid_image_file($file_path)) {
                    $this->handle_image_file($file_path, $file);
                }
            } else {
                $file->size = $file_size;
                if (!$content_range && $this->options['discard_aborted_uploads']) {
                    unlink($file_path);
                    $file->error = $this->get_error_message('abort');
                }
            }
            $this->set_additional_file_properties($file);
        }
        return $file;
    }
}
